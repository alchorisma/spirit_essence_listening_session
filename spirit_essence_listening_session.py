"""
Videmus haec signa numquam fere mentientia nec tamen cur ita fiat videmus.
- Cicero, de divinatione Livre 1, IX.
"""

import os
import shlex
import subprocess

import numpy

from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import VGG16
from keras import models


FILENAME_INPUT = os.environ.get('FILENAME_INPUT')


def get_output_layer():
    """
    Duo sunt enim divinandi genera, quorum alterum artis est, alterum naturae.
    Quae est autem gens aut quae civitas quae non aut extispicum aut monstra aut fulgora interpretantium,
    aut augurum, aut astrologorum, aut sortium (ea enim fere artis sunt),
    aut somniorum aut vaticinationum (haec enim duo naturalia putantur), praedictione moveatur?
    Quarum quidem rerum eventa magis arbitror quam causas quaeri oportere.
    Est enim vis et natura quaedam, quae tum observatis longo tempore significationibus,
    tum aliquo instinctu inflatuque divino futura praenuntiat.
    - Cicero, de divinatione Livre 1, VII
    """
    # load the model
    model = VGG16()
    model.summary()
    # load an image from file
    image = load_img(FILENAME_INPUT, target_size=(224, 224))
    # convert the image pixels to a numpy array
    image = img_to_array(image)
    image = numpy.expand_dims(image, axis=0)
    image /= 255.

    layer_outputs = [layer.output for layer in model.layers[1:18]]
    activation_model = models.Model(inputs=model.input, outputs=layer_outputs)
    activations = activation_model.predict(image)
    layer = 0  # we choose the first layer
    layer_activation = activations[layer]
    print(layer_activation.shape)
    ide, width, height, channels = layer_activation.shape
    print(channels)
    string = ''
    l_average = []
    for channel in range(channels):
        average = numpy.average(layer_activation[0, :, :, channel].tolist())
        l_average.append(average)
        l_list_of_pixels_line = []
        for line in layer_activation[0, :, :, channel].tolist():
            l_list_of_pixels_line.extend(line)
        string += ' '.join(map(str, l_list_of_pixels_line))
        string += '\n'
    filename = '{}_channels_of_layer_{}'.format(channels, layer)
    # save data in a file for Pd
    with open(filename, 'w') as f:
        f.write(string)
    with open(filename + '_average', 'w') as f:
        f.write(' '.join(map(str, l_average)))
    return filename, width, height


def start_pd(filename, width, height):
    """
    Atque etiam ventos praemonstrat saepe futuros inflatum mare, cum subito penitusque tumescit,
    saxaque cana salis niveo spumata liquore tristificas certant Neptuno reddere voces,
    aut densus stridor cum celso e vertice montis ortus adaugescit scopulorum saepe repulsus.
    - Cicero, de divinatione Livre 1, VII
    """
    # cmd_pd = 'pd -open main.pd -send ";filename {}-{}-{}-{}"'.format(filename, width, height, FILENAME_INPUT)
    cmd_pd = 'pd -open main.pd -audiooutdev 1 -outchannels 2 -send ";filename {}-{}-{}-{}"'.format(filename, width, height, FILENAME_INPUT)
    cmd_pd2 = 'pd -open main_gem.pd -send ";filename {}-{}"'.format(filename, FILENAME_INPUT)
    sub_pd = subprocess.Popen(shlex.split(cmd_pd))
    sub_pd2 = subprocess.Popen(shlex.split(cmd_pd2))
    sub_pd.wait()
    sub_pd2.wait()


def main():
    """
    Nam et natura significari futura sine deo possunt,
    et ut sint di potest fieri ut nulla ab eis divinatio generi humano tributa sit.
    - Cicero, de divinatione Livre 1, VI
    """
    filename, width, height = get_output_layer()
    start_pd(filename, width, height)


main()
